package project;

import java.util.Scanner;

abstract class item {
	
	private double value;
	private String name, addInfo;
	private static Scanner input = new Scanner(System.in);
	
	public item() {}
	
	public item(String n, double v, String a) {
		value = v; name = n; addInfo = a; 
	}
	
	public item(String[] a) {
		value = Double.parseDouble(a[1]);
		name = a[0]; addInfo = a[2];
	}

	//comes in later for the subclasses
	
	//puts item into their specific stock section
	abstract void stock();
	
	//removes item from specific stock section
	abstract void remove();
	
	
	//getters
	public double getValue() {
		return value;
	}
	
	public String getName() {
		return name;
	}	

	public String getAddInfo() {
		return addInfo;
	}
	
	public static Scanner input() {
		return input;
	}
	
	//setters
	public void setValue(double value) {
		this.value = value;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void setAddInfo(String addInfo) {
		this.addInfo = addInfo;
	}

	//toString
	public String toString() {
		return name+": [Value - "+value+"] [Info - "+addInfo+"]";
	}
	
	//specialized creation function
	abstract item create();

	
}
