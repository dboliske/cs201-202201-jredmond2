package project;

public class alcohol extends item {

	public alcohol() {
		// TODO Auto-generated constructor stub
	}

	public alcohol(String n, double v, String a) {
		super(n, v, a);
		// TODO Auto-generated constructor stub
	}

	public alcohol(String[] a) {
		super(a);
		// TODO Auto-generated constructor stub
	}
	
	void stock() {
		mainClass.alcoholStock.add(this);
	}

	void remove() {
		mainClass.alcoholStock.remove(this);
	}
	
	public String toString() {
		return super.getName()+": [Value - "+super.getValue()+"] [Age Requirement - "+super.getAddInfo()+"]";
	}

	public item create() {

		System.out.print("Enter name of new alcohol: ");
		super.setName(super.input().next());
		System.out.print("Enter price of new alcohol: ");
		super.setValue(mainClass.getDouble(super.input()));
		System.out.print("Enter age requirement: ");
		super.setAddInfo(super.input().next());
		
		return this;
	}
}
