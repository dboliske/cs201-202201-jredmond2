# Final Project

## Total

150/150

## Break Down

Phase 1:                                                            50/50 #Updated after code

- Description of the user interface                                 3/3
- Description of the programmer's tasks:
  - Describe how you will read the input                            3/3
  - Describe how you will process the data from the input file      4/4
  - Describe how you will store the data                            3/3
  - How will you add/delete/modify data?                            0/5
  - How will you search data?                                       5/5
- Classes: List of names and descriptions                           7/7
- UML Class Diagrams                                                5/10
- Testing Plan                                                      0/10

Phase 2:                                                            100/100

- Compiles and runs with no run-time errors                         10/10
- Documentation                                                     5/15
- Test plan                                                         10/10
- Inheritance relationship                                          5/5
- Association relationship                                          5/5
- Searching works                                                   5/5
- Uses a list                                                       5/5
- Project reads data from a file                                    5/5
- Project writes data to a file                                     5/5
- Project adds, deletes, and modifies data stored in list           5/5
- Project generates paths between any two stations                  -/10 #RUBRIC IGNORED
- Project encapsulates data                                         5/5
- Project is well coded with good design                            10/10
- All classes are complete (getters, setters, toString, equals...)  5/5


## Comments
This is a really good project. Yet, the best project I have seen.
Very organized, very interactive, fun and realistic.
Very good work Jeremy! Keep it up.
### Design Comments
-Design looks okay but there was no description of how the data will be added, modifies, or deleted.
-There was no UML diagram. The diagram you provided gives an idea about the relationship between your classes, but it is neither clear nor detailed. You were asked to provide a UML diagram as shown in class.
-Testing plan missing.

### Code Comments
Your application class is very interactive and fun. Good job. I really enjoyed running your code
Documentation missing -10 points
+10 points for all extra methods. I loved the browse item method and how you categorized your items.
Phase 1 grade updated because your code runs perfectly and meets all the expectations