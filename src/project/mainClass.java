package project;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class mainClass {

	public static stock foodStock = new stock("Food");
	public static stock stationeryStock = new stock("Stationery");
	public static stock drinkStock = new stock("Drink");
	public static stock alcoholStock = new stock("Alcohol");
	public static stock medicineStock = new stock("Medicine");

	public static stock[] stocks = { foodStock, stationeryStock, drinkStock, alcoholStock, medicineStock };
	public static String[] stockNames = { foodStock.getName(), stationeryStock.getName(), drinkStock.getName(), alcoholStock.getName(), medicineStock.getName() };
	
	public static store market = new store();
	public static cart shoppingCart = new cart();

	public static Scanner input = new Scanner(System.in);
	public static File currentFile;

	public static boolean loadedFile = false;

	public static void main(String[] args) {

		boolean shopping = true;

		while (shopping) {
			
			switch (mainMenu()) {
			case 1:
				loadSave();
				break;
			case 2:
				saveFile();
				break;
			case 3:
				customer();
				break;
			case 4:
				stocker();
				break;
			case 5:
				input.close();
				shopping = false;
				break;
			}
		}
		System.out.println("Bye-Bye!");
	}

	public static void loadSave() {
		Scanner fileInput;
		File readIn;
		try {

			System.out.println("Enter name of file");
			input = new Scanner(System.in);
			readIn = new File("src/project/" + input.next() + ".csv");
			fileInput = new Scanner(readIn);

			String[] line = fileInput.nextLine().split(",");
			String[] sections = { "Food", "Stationery", "Drinks", "Alcohol", "Medicine" };
			item tempItem = null;

			for (int i = 0; i < sections.length; i++) {
				while (!line[0].equals("&*$#")) {

					switch (i) {
					case 0:
						tempItem = new food(line);
						break; // food
					case 1:
						tempItem = new stationery(line);
						break; // stationery
					case 2:
						tempItem = new drinks(line);
						break; // drinks
					case 3:
						tempItem = new alcohol(line);
						break; // alcohol
					case 4:
						tempItem = new medicine(line);
						break; // medicine
					}

					tempItem.stock();
					line = fileInput.nextLine().split(",");
				}
				if (fileInput.hasNext()) {
					line = fileInput.nextLine().split(",");
				}
			}

			market.setProfitValue(Double.parseDouble(line[1]));
			market.setCapital(Double.parseDouble(line[2]));

			System.out.println("File " + readIn.getName() + " loaded" + "\n");
			currentFile = readIn;
			loadedFile = true;

		} catch (FileNotFoundException f) {
			System.out.println("no file lol");
		}

	}

	public static void saveFile() {
		FileWriter printer = null;
		File saveFile = null;

		try {
			System.out.println("1/Create new file");
			if (loadedFile) {
				System.out.println("2/Override current file");
			}

			switch (getInt(input)) {
			case 2:
				if (loadedFile) {
					saveFile = currentFile;
					printer = new FileWriter(saveFile);
					System.out.println("File Saved");
					break;
				}
			case 1:

				System.out.println("Enter name of file");
				saveFile = new File("src/project/" + input.next() + ".csv");
				if (saveFile.createNewFile()) {
					System.out.println("File created");
				} else {
					System.out.println("File already exists");
				}
				printer = new FileWriter(saveFile);

				break;
			}

			int count = 0;
			for (stock s : stocks) {
				for (item i : s.getStock()) {
					printer.write(i.getName() + "," + i.getValue() + "," + i.getAddInfo() + "\n");
				}
				count++;
				if(count == stocks.length) {
					printer.write("&*$#,"+market.getProfitValue()+","+market.getCapital()+"\n");
				}
				else {
				printer.write("&*$#\n");}
			}

			
			printer.flush();
			printer.close();
		} catch (IOException e) {
			System.out.println("Death");
		}
	}

	public static void customer() {
		boolean shopping = true;
		while(shopping) {
			System.out.println("Customer Mode");
		String[] options = {"Search Product", "Browse Section", "Check Cart", "Checkout"};
		switch(createMenu(options)) {
		case 1: shoppingCart.add(searchProduct()); break;
		case 2: shoppingCart.add(browseProduct()); break;
		case 3: System.out.println(shoppingCart.getObjects(market.getProfitValue())); break;
		case 4: market.addCapital(shoppingCart.checkout(market.getProfitValue())); shopping = false; break;
		}
		
		}
	}
	
	public static void stocker() {
		boolean working = true;
		while(working) {
		System.out.println("Stocker Mode \nStore Capital: "+Math.round(market.getCapital()*100.00)/100.00);
		String[] options = {"Stock Product", "Change Profit Margin", "Check/Alter Stock", "Finish Shift"};
		switch(createMenu(options)) {
		case 1: stockProduct(); break;
		case 2: System.out.println("Enter new store profit margin (current: "+market.getProfitValue()+")"); market.setProfitValue(getDouble(input)); break;
		case 3: alterItem(); break;
		case 4: working = false; break;
		}
		
		}
		
	}
	
	//yes, the hardcode is disgusting
	//couldn't think of a better system
	public static void stockProduct() {
		item newProduct;
		stock chosenStock = chooseStock();
		
		switch(chosenStock.getName()) {
		case "Food": newProduct = new food().create(); break;
		case "Stationery": newProduct = new stationery().create(); break;
		case "Alcohol": newProduct = new alcohol().create(); break;
		case "Drinks": newProduct = new drinks().create(); break;
		case "Medicine": newProduct = new medicine().create(); break;
		default: newProduct = null; break;
		}
		
		chosenStock.add(newProduct);
		System.out.println(newProduct.getName()+" has been added to the "+chosenStock.getName()+" stock list.");
	}
	
	public static void alterItem() {
		item changedItem = browseProduct();
		if(changedItem != null) {
		changedItem = changedItem.create();
		changedItem.stock();
		}
		
	}
	
	public static item searchProduct() {
		System.out.println("Enter name of product: ");
		String selection = input.next();
		ArrayList<item> matches = new ArrayList<item>();
		matches.clear();
		
		for(stock s: stocks) {
			for(item i: s.getStock()) {
				if(i.getName().equals(selection)) {
					matches.add(i);
				}
			}
		}
		
		if(matches.size()>0) {
			System.out.println(matches.size()+" item(s) found. Choose this item? y/1 n/2");
			switch(getInt(input)) {
				case 1: matches.get(0).remove(); return matches.get(0);
				default: System.out.println("No matches found"); return null;
				}
			}
		else{
			System.out.println("No matches found");
			return null;
		}
	}
	
	public static item browseProduct() {
		
		stock chosenStock = chooseStock();
		
		System.out.println("0/Return");
		int itemIndex = createMenu(chosenStock.getContents(market.getProfitValue()).split("\n"))-1;
		if(itemIndex != -1) {
		item chosenItem = chosenStock.getItem(itemIndex);
		chosenStock.remove(chosenItem);
		System.out.println(chosenItem.getName()+" has been chosen");
		return chosenItem;}
		else
		{return null;}
	}
	
	public static stock chooseStock() {
		return stocks[createMenu(stockNames)-1];
	}
	
	public static int mainMenu() {
		System.out.println("Main Menu");
		//yes this is redundant, I had to change code in the future and didn't want to change too much
		String[] options = { "Load File", "Save File", "Enter Store as Customer", "Enter Store as Stocker", "Exit" };
		return createMenu(options);
	}
	
	public static int getInt(Scanner input) {
		int value = 0;
		boolean trying = true;
		while(trying) {
		try {
		value = Integer.parseInt(input.next());
		trying = false;
		}
		catch(InputMismatchException e) {System.out.println("Please enter an integer value"); trying = true;}
		catch(NumberFormatException e) {System.out.println("Please enter an integer value"); trying = true;}
		}
		return value;
	}
	
	public static double getDouble(Scanner input) {
		double value = 0;
		boolean trying = true;
		
		while(trying) {
		try {
		value = Double.parseDouble(input.next());
		trying = false;
		}
		catch(InputMismatchException e) {System.out.println("Please enter a double value"); trying = true;}
		catch(NumberFormatException e) {System.out.println("Please enter an double value"); trying = true;}
		}
		return value;
	}
	
	public static int createMenu(String[] opts) {
		int count = 0;
			count = 0;
		for(String s: opts) {
			count++;
			System.out.println(count+ "/" + s);
		}
		while(true) {
		int value = getInt(input);
		if(value <= opts.length && value > -1) {
		return value;}
		else {System.out.println("Please enter a possible value");}
		}
	}
}