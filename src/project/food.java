package project;

public class food extends item{

	public food() {
		super();
	}
	
	public food( String n, double d, String a) {
		super(n,d,a);
	}
	
	public food(String[] a) {
		super(a[0], Double.parseDouble(a[1]),a[2]);
	}

	void stock() {
		mainClass.foodStock.add(this);
	}

	void remove() {
		mainClass.foodStock.remove(this);
	}
	
	public String toString() {
		return super.getName()+": [Value - "+super.getValue()+"] [Expiration Date - "+super.getAddInfo()+"]";
	}

	public item create() {

		System.out.print("Enter name of new food: ");
		super.setName(super.input().next());
		System.out.print("Enter price of new food: ");
		super.setValue(mainClass.getDouble(super.input()));
		System.out.print("Enter expiration date (MM/DD/YYYY): ");
		super.setAddInfo(super.input().next());
		
		return this;
	}
	
}
