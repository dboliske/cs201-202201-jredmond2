package project;

public class store {
	
	//default values
	double capital = 500.00;
	double profitValue = 1.1;
	
	public store() {
		// TODO Auto-generated constructor stub
	}

	public double getCapital() {
		return capital;
	}
	
	public void addCapital(double capital) {
		this.capital += capital;
	}
	
	public void subCapital(double capital) {
		this.capital -= capital;
	}

	public void setCapital(double capital) {
		this.capital = capital;
	}

	public double getProfitValue() {
		return profitValue;
	}

	public void setProfitValue(double profitValue) {
		this.profitValue = profitValue;
	}

}
