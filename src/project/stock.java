package project;

import java.util.ArrayList;

public class stock{
	
	ArrayList<item> stock = new ArrayList<item>();
	
	public stock() {}
	
	public stock(String n) {name = n;}
	
	String name;
	
	public String getName() {return name;}

	public int size() {return stock.size();}
	
	public String getContents(double value) {
		String r = "";
		
		for(item i: stock) {
			r += i.getName()+" - Price: $"+(Math.round((i.getValue()*value)*100.00)/100.00)+"\n";
		}
		
		return r;
	}
	
	public ArrayList<item> getStock()
	{
		return stock;
	}
	
	public item getItem(int index) {
		return stock.get(index);
	}
	
	public int findItem(item obj) {
		return stock.indexOf(obj);
	}
	
	public void add(item obj) {
		stock.add(obj);
	}
	
	public void remove(item obj) {
		stock.remove(obj);
	}
}