package project;

public class stationery extends item{

	public stationery() {
		super();
	}
	
	public stationery( String n, double d, String a) {
		super(n,d,a);
	}
	
	public stationery(String[] a) {
		super(a[0], Double.parseDouble(a[1]),"");
	}

	void stock() {
		mainClass.stationeryStock.add(this);
	}

	void remove() {
		mainClass.stationeryStock.remove(this);
	}
	
	public String toString() {
		return super.getName()+": [Value - "+super.getValue()+"]";
	}
	
	public item create() {

		System.out.print("Enter name of new stationery: ");
		super.setName(super.input().next());
		System.out.print("Enter price of new stationery: ");
		super.setValue(mainClass.getDouble(super.input()));
		
		return this;
	}
}
