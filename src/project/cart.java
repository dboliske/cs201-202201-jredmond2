package project;

import java.util.ArrayList;

public class cart {

	ArrayList<item> shoppingCart = new ArrayList<item>();
	
	public cart() {
		// TODO Auto-generated constructor stub
	}
	
	public void add(item obj) {
		if(obj!=null) {
		shoppingCart.add(obj);}
	}
	
	public String getObjects(double value) {
		String objectList = "";
		for(item i: shoppingCart) {
			objectList += "\n"+i.getName()+" - Price: $"+(Math.round((i.getValue()*value)*100.00)/100.00);
		}
		return objectList;
	}
	
	public double checkout(double value) {
		double d = 0;
		for(item i: shoppingCart) {
			d += i.getValue()*value;
		}
		
		shoppingCart.clear();
		System.out.println("Paid $"+(Math.round(d*100.00)/100.00)+" at checkout. Have a nice day!");
		return d;
	}
	
	public double getValue() {
		double d = 0;
		for(item i: shoppingCart) {
			d += i.getValue();
		}
		return d;
	}


	
	
}
