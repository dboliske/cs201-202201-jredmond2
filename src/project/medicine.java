package project;

public class medicine extends item{

	public medicine() {
		super();
	}
	
	public medicine( String n, double d, String a) {
		super(n,d,a);
	}
	
	public medicine(String[] a) {
		super(a[0], Double.parseDouble(a[1]),a[2]);
	}

	void stock() {
		mainClass.medicineStock.add(this);
	}

	void remove() {
		mainClass.medicineStock.remove(this);
	}
	
	public String toString() {
		return super.getName()+": [Value - "+super.getValue()+"] [Age Requirement - "+super.getAddInfo()+"]";
	}
	
	public item create() {

		System.out.print("Enter name of new medicine: ");
		super.setName(super.input().next());
		System.out.print("Enter price of new medicine: ");
		super.setValue(mainClass.getDouble(super.input()));
		System.out.print("Enter age requirement: ");
		super.setAddInfo(super.input().next());
		
		return this;
	}
}
