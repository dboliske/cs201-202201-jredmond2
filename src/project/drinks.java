package project;

public class drinks extends item{

	public drinks() {
		super();
	}
	
	public drinks( String n, double d, String a) {
		super(n,d,a);
	}
	
	public drinks(String[] a) {
		super(a[0], Double.parseDouble(a[1]),a[2]);
	}
	
	void stock() {
		mainClass.drinkStock.add(this);
	}

	void remove() {
		mainClass.drinkStock.remove(this);
	}
	
	public String toString() {
		return super.getName()+": [Value - "+super.getValue()+"] [Expiration Date - "+super.getAddInfo()+"]";
	}
	
	public item create() {

		System.out.print("Enter name of new drink: ");
		super.setName(super.input().next());
		System.out.print("Enter price of new drink: ");
		super.setValue(mainClass.getInt(super.input()));
		System.out.print("Enter expiration date (MM/DD/YYYY): ");
		super.setAddInfo(super.input().next());
		
		return this;
	}
}
