package exams.second;

import java.util.Scanner;

public class search {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		double[] values = {0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142};
		System.out.println("Input a double, please. I really don't wanna bother with more try and catch stuff.");
		System.out.println(searchValue(values, input.nextDouble()));
	}

	public static int searchValue(double[] values, double val) {
		int pos = 0;
		int jumpDistance = (int)Math.sqrt(values.length);
		
		while(pos<=values.length) {
			if(values[pos] == val) {return pos;}
			if(values[pos] > val) {jumpDistance = -1;}
			pos += jumpDistance;
		}
		
		return -1;
	}
}
