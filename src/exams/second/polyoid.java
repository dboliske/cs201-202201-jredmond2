package exams.second;

public abstract class polyoid {
	//aka Polygon

	String namoid;
	double areoid;
	double perimitoid;
	
	public polyoid() {}

	//i did these getters and setters manually because i decided on the dumbest variable names imaginable
	public String getName() {
		return namoid;
	}
	
	public double getArea() {
		return areoid;
	}
	
	public double getPerim(){
		return perimitoid;
	}

	public void setName(String n) {
		namoid = n;
	}
	
	public void setArea(double a) {
		areoid = a;
	}
	
	public void setPerimeter(double p) {
		perimitoid = p;
	}
	
	abstract double perimeter();
	
	abstract double area();
	
	public String toString() {
		return "Polygon "+namoid+": Area - "+areoid+" Perimeter - "+perimitoid;
	}
}
