package exams.second;

public class sorting {

	public static void main(String[] args) {
		
		String[] values = {"speaker", "poem", "passenger", "tale", "reflection", "leader", "quality", "percentage", "height", "wealth", "resource", "lake", "importance"};
		values = sortArray(values);
		System.out.println(getArrayContents(values));
	}

	public static String[] sortArray(String[] values) {
		
		String[] newValues = new String[values.length];
		String highest = values[0];
		int highestIndex = 0;
		
		for(int j=0; j<values.length; j++) {
			for(int i=0; i<values.length; i++) {
				if(compareStrings(values[i],highest) < 0) {
					highest = values[i];
					highestIndex = i;
				}
			}
			newValues[j]=highest;
			values[highestIndex] = null;
			highestIndex = newHighest(values);
			highest = values[highestIndex];
		
		}
		
		return newValues;
	}
	
	public static int compareStrings(String one, String two) {
		if(one == null||two == null) {
			return 0;
		}
		else {
		int comparison = one.compareTo(two);
		return comparison;}
	}
	
	public static int newHighest(String[] values) {
		for(int i=0; i<values.length; i++) {
			if(values[i] != null) {
				return i;
			}
		}
		return 0;
	}
	
	public static String getArrayContents(String[] values) {
		String exit = "";
		for(String i: values) {
			exit += i+", ";
		}
		return exit;
	}
}
