package exams.second;

public class classroom {

	String building;
	String roomNum;
	int seats;
	
	public classroom() {}

	//is generating the getters and setters cheating?
	//cuz like, its so damn boring i'm sorry
	//i mean you probably wouldn't have known if i never told you
	//but like, y'know. I modified the toString though since the generated one sucks
	
	public String getBuilding() {
		return building;
	}

	public void setBuilding(String building) {
		this.building = building;
	}

	public String getRoomNum() {
		return roomNum;
	}

	public void setRoomNum(String roomNum) {
		
		this.roomNum = roomNum;
	}

	public int getSeats() {
		return seats;
	}

	public void setSeats(int seats) {
		if(seats < 0) {
			seats = 0;
		}
		this.seats = seats;
	}

	
	public String toString() {
		return "classroom ["+building + ": " + roomNum + ", seats: " + seats + "]";
	}
	
	

}
