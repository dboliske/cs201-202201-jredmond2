# Final Exam

## Total

90/100

## Break Down

1. Inheritance/Polymorphism:    18/20
    - Superclass:               5/5
    - Subclass:                 5/5
    - Variables:                3/5
    - Methods:                  5/5
2. Abstract Classes:            17/20
    - Superclass:               4.5/5
    - Subclasses:               4/5
    - Variables:                3.5/5
    - Methods:                  5/5
3. ArrayLists:                  20/20
    - Compiles:                 5/5
    - ArrayList:                5/5
    - Exits:                    5/5
    - Results:                  5/5
4. Sorting Algorithms:          20/20
    - Compiles:                 5/5
    - Selection Sort:           10/10
    - Results:                  5/5
5. Searching Algorithms:        15/20
    - Compiles:                 5/5
    - Jump Search:              5/10
    - Results:                  5/5

## Comments

1.Instance variables not properly defined (private/protected)
2.wrong class names, instance variables not properly defined.
3.Good
4.Good
5.Jump search should have been recursive
