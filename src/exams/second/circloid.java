package exams.second;

public class circloid extends polyoid{
	//aka circle
	
	//this sounds like an actual math term
	//it isn't tho
	double radioid;
	
	public circloid() {
		super();
	}
	
	public void setRadius(double r){
		if(r <= 0) {r = 1;}
		radioid = r;
	}

	public double getRadius() {
		return radioid;
	}
	
	public String toString() {
		return "Circle "+namoid+": R - "+radioid+"  A - "+areoid+"  P - "+perimitoid;
	}
	
	//didn't have to look this one up, trust me *wink*
	public double area() {
		double area = Math.PI*(radioid*radioid);
		setArea(area);
		return area;
	}
	
	public double perimeter() {
		double perimeter = 2*Math.PI*radioid;
		setPerimeter(perimeter);
		return perimeter;
	}
	
	//i now realize you put the equations in the ReadMe
	//joke ruined ;-;
}
