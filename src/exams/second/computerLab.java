package exams.second;

public class computerLab extends classroom {

	boolean computers;
	
	public computerLab() {
		super();
	}

	public boolean hasComputers() {
		return computers;
	}

	public void setComputers(boolean computers) {
		this.computers = computers;
	}

	
	public String toString() {
		return "computer lab ["+building + ": " + roomNum + ", seats: " + seats + ", computers: "+computers+"]";
	}

	
	
}
