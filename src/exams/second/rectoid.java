package exams.second;

public class rectoid extends polyoid {
	//aka rectangle
	
	double widthoid;
	double heightoid;
	
	public rectoid() {
		super();
	}

	public double getWidth() {
		return widthoid;
	}
	
	public double getHeight() {
		return heightoid;
	}
	
	public void setWidth(double w) {
		if(w <= 0) {w = 1;}
		widthoid = w;
	}
	
	public void setHeight(double h) {
		if(h <= 0) {h = 1;}
		heightoid = h;
	}
	
	public String toString() {
		return "Retangle "+namoid+": W - "+widthoid+"  H - "+heightoid+"  A - "+areoid+"  P - "+perimitoid;
	}
	
	//i had to look these equations up, they were a toughie
	public double area() {
		double area = widthoid*heightoid;
		setArea(area);
		return area;
	}
	
	public double perimeter() {
		double perimeter = 2*(widthoid+heightoid);
		setPerimeter(perimeter);
		return perimeter;
	}
}
