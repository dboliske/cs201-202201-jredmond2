package labs.lab3;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.FileWriter;


public class ArrayOfDestiny2 {

	public static File output;
	public static FileWriter FileWriter;
	public static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		System.out.println("Input a series of numbers seperated by [Enter]. Input \"done\" to finish.");
		
		String userInput = "";
		ArrayList<Integer> arr = new ArrayList<Integer>();
		
		while(!userInput.equals("done")) {
			userInput = input.next();
			if(!userInput.equals("done")) {
				arr.add(Integer.parseInt(userInput));
			}
		}
		
		System.out.println("Input file name (with.txt)");
		output = new File("src/labs/lab3/"+input.next());
		
		boolean created = false;
		try {
			created = output.createNewFile();
			FileWriter = new FileWriter(output);
		} catch (FileNotFoundException e) {
			System.out.println("e");
		} catch (IOException e) {
			System.out.println("Whatever an IOException is");
		}
		
		for(int i: arr) {
			writeToFile(output, i+"");
		}
		
		try {
			FileWriter.flush();
			FileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if(created) {
			System.out.println("File Successfully created");
		}
		else {
			System.out.println("File Successfully not created");
		}
	}

	public static void writeToFile(File output, String input) {
		try {
			FileWriter.write(input+"\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
