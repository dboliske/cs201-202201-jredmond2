# Lab 3

## Total

17/20

## Break Down

* Exercise 1    5.5/6
* Exercise 2    5.5/6
* Exercise 3    6/6
* Documentation 0/2

## Comments
Ex1: You needed to output the exact average as a double. The integer output is not exact.
Ex2: You needed to use an array and then create another array within your code that increments the size as long as there is more data. 
Your code is fine.