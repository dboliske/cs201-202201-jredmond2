package labs.lab3;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ArrayOfDestiny {

	//just some variables, they're kinda cool I guess.
	public static File grades = new File("src/labs/lab3/grades.csv");
	public static Scanner excelInput;
	public static String[] names = new String[14];
	public static int[] scores = new int[14];
	public static int row = 0;
	

	public static void main(String args[]) {
		//creates excelInput scanner. I made the scanner static so I wouldn't have to put the entire damn program in a try statement
		try {
			excelInput = new Scanner(grades);
			//associated catch statement with a very creative output
		} catch (FileNotFoundException e) {
			System.out.println("No File Found");
		}
		
		//just loops until the spreadsheet is dead
		while(excelInput.hasNext()) {
			//creates "dummy" string array with no size, because size doesn't matter. That's what my girlfriend said.
			
			String[] split;
			
			/*since the excel spreadsheet is weirdly formatted, I have to combine the first two strings into one in order
			to make it coherent.
			This code looks disgusting, it makes me want to throw up. But it's what I got to do.*/
			
			String nameGrade = excelInput.next() + excelInput.next();
			//the dummy string array is filled by a split array taking the name and the score seperately
			split = nameGrade.split(",");
			//puts the name and score into their respective arrays
			names[row] = split[0];
			scores[row] = Integer.parseInt(split[1]); //parse int cuz i said so
			//increases current row
			row++;
		}
		//we are leaving the while statement, say goodbye to the while statement everyone. "Goodbye!"
		//A simple For Loop that totals up all the scores.
		int tot = 0;
		
		for(int score: scores) {
			tot += score;
		}
		//aaaaaand print
		System.out.println("Class Average: "+tot/scores.length);
		
		//I do the comments cuz it makes me look smart
		//big brain
	}
}
