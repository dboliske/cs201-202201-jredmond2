package labs.lab7;

public class selectionSort {

	public static void main(String[] args) {

		double[] library = { 3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282 };
		double min = 999.9;
		int mindex = 0;
		for (int k = 0; k < library.length; k++) {
			min = 999.9;
			for (int i = k; i < library.length; i++) {
				if (library[i] < min) {
					min = library[i];
					mindex = i;
				}

			}
			library[mindex] = library[k];
			library[k] = min;
		}
		
		for(double i: library) {
			System.out.print(i+", ");
		}
	}

}
