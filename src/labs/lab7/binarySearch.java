package labs.lab7;

import java.util.Scanner;

public class binarySearch {

	public static Scanner input = new Scanner(System.in);
	public static String[] library = {"c", "html", "java", "python", "ruby", "scala"};
	public static int interval = (int)(Math.sqrt(library.length));
	
	public static void main(String[] args) {
		System.out.println("Type thing: ");
		System.out.println(getIndex(input.next()));
		
	}

	public static String getIndex(String inp) {
		int check = library.length/2;
		for(int i = 0; i<library.length/interval; i++) {
			if(library[check].equalsIgnoreCase(inp)) {
				return "Index: "+check;
			}
			else
			{
				if(library[check].compareToIgnoreCase(inp)>0) {
					check -= interval;
				}
				else {check += interval;}
				interval/=2;
				if(interval<1) {interval=1;}
			}
		}
		
		return "Index: ";
	}
	
}
