package labs.lab7;

public class bubbleSort {

	public static void main(String[] args) {

		int[] intArr = { 10, 4, 7, 3, 8, 6, 1, 2, 5, 9 };

		printArr(sortArr(intArr));

	}

	public static void printArr(int[] arr) {
		for (int i : arr) {
			System.out.print(i + ", ");
		}
	}

	public static int[] sortArr(int[] arr) {
		int temp;

		for (int j = 0; j < arr.length; j++) {
			for (int i = 0; i < arr.length - 1; i++) {
				if (arr[i + 1] < arr[i]) {
					temp = arr[i + 1];
					arr[i + 1] = arr[i];
					arr[i] = temp;
				}
			}
		}
		return arr;
	}
}
