package labs.lab5;

import labs.lab5.GeoLocation;

public class GeoLocation {

	public double lat;
	public double lng;

	GeoLocation() {
	}

	GeoLocation(double lat, double lng) {
		this.lat = lat;
		this.lng = lng;
	}

	public double getLng() {
		return lng;
	}

	public double getLat() {
		return lat;
	}
	
	public void setLng(double lng) {
		this.lng = lng;
	}
	
	public void setLat(double lat) {
		this.lat = lat;
	}

	public String toString() {
		return lat + "," + lng;
	}

	public boolean validLat() {
		if (lat > -90.0 && lat < 90.0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean validLng() {
		if (lng > -180.0 && lng < -180.0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean equals(GeoLocation comparison) {
		if (this.equals(comparison)) {
			return true;
		} else {
			return false;
		}
	}

	public double getDistance(GeoLocation obj) {
		double distance = 0;
		distance += Math.pow(lat - obj.getLat(), 2);
		distance += Math.pow(lng - obj.getLng(), 2);
		distance = Math.sqrt(distance);
		return distance;
	}
	
}
