# Lab 5

## Total

15.5/20

## Break Down

CTAStation

- Variables:                    1.5/2
- Constructors:                 0.5/1
- Accessors:                    1.5/2
- Mutators:                     2/2
- toString:                     1/1
- equals:                       2/2

CTAStopApp

- Reads in data:                2/2
- Loops to display menu:        2/2
- Displays station names:       0.5/1
- Displays stations by access:  0/2
- Displays nearest station:     1.5/2
- Exits                         1/1

## Comments
*CTAStation:
-The variables are not private
-Default constructor should return default values
-One of the accessors does not match the UML
*CTAStation:
-Your program should only display the stations' names
-Your program does not display stations by access
-Your program displays nearest station, but it shouls not ask for the latitude and longitude at the beginning
