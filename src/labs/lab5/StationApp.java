package labs.lab5;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class StationApp {

	public static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		int numStat = 34;
		int line = 0;
		boolean using = true;
		boolean wheelChairMode = false;

		CTAStation[] stations = new CTAStation[numStat];
		CTAStation newStation;
		GeoLocation userLocation = new GeoLocation();

		Scanner fileInput = null;
		File inputFile = null;
		String[] vars = new String[6];
		

		try {
			inputFile = new File("src/labs/lab5/CTAStops.csv");
			fileInput = new Scanner(inputFile);
		} catch (FileNotFoundException e) {
			print("File not found");
		}

		fileInput.next();

		while (line < numStat) {
			vars = new String[6];
			vars = fileInput.next().split(",");
			newStation = new CTAStation(vars[0], vars[3], Boolean.parseBoolean(vars[4]), Boolean.parseBoolean(vars[5]));
			newStation.setLat(Double.parseDouble(vars[1]));
			newStation.setLng(Double.parseDouble(vars[2]));
			stations[line] = newStation;
			line++;
		}

		print("Input personal Latitude: ");
		userLocation.setLat(input.nextDouble());
		print("Input personal Longitude: ");
		userLocation.setLng(input.nextDouble());
		
		while (using) {
			
			String[] opts = { "Display Stations", "Toggle Wheelchair Mode [" + wheelChairMode + "]", "Show Nearest Station",
			"Exit" };
			switch (showMenu(opts)) {
			case 1:
				ShowStations(stations,wheelChairMode);
				break;
			case 2: wheelChairMode = toggleBoolean(wheelChairMode); break;
			case 3:
				System.out.println("\n "+" Nearest Station:" + findNearest(stations, userLocation) + "\n");
				break;
			case 4: using = false; print("lol u missed ur train");
			default:
			}
		}

		fileInput.close();
	}

	public static void print(String a) {
		System.out.println(a);
	}

	public static int showMenu(String[] options) {

		for (int i = 0; i < options.length; i++) {
			print((i + 1) + "/" + options[i]);
		}

		return input.nextInt();
	}

	public static void ShowStations(CTAStation[] stations, boolean wheelchair) {
		if (!wheelchair) {
			for (CTAStation station : stations) {
				print(station + "");
			}
		} else {
			for (CTAStation station : stations) {
				if (station.isWheelchair()) {
					print(station + "");
				}
			}
		}
	}

	public static boolean toggleBoolean(boolean boo) {
		if(boo) {return false;}
		else {return true;}
	}
	
	public static CTAStation findNearest(CTAStation[] stations, GeoLocation userLocation) {
		CTAStation near = stations[0];

		for (CTAStation station : stations) {
			if (station.getDistance(userLocation) < near.getDistance(userLocation)) {
				near = station;
			}
		}

		return near;
	}

}
