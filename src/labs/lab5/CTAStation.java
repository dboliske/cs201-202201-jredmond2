package labs.lab5;

import java.util.Objects;

public class CTAStation extends GeoLocation {

	String name;
	String location;
	boolean wheelchair;
	boolean open;
	
	public CTAStation() {}
	
	public CTAStation(String name, String location, boolean wheelchair, boolean open) {
		this.name = name;
		this.location = location;
		this.wheelchair = wheelchair;
		this.open = open;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
	
	public double getLat() {return this.lat;}
	public double getLng() {return this.lng;}

	public boolean isWheelchair() {
		return wheelchair;
	}

	public void setWheelchair(boolean wheelchair) {
		this.wheelchair = wheelchair;
	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}

	public String toString() {
		return name+"\n  Location - "+this.location+"\n  Wheelchair Accessible - "+wheelchair+"\n  Open - "+open;
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CTAStation other = (CTAStation) obj;
		return Objects.equals(location, other.location) && Objects.equals(name, other.name) && open == other.open
				&& wheelchair == other.wheelchair;
	}

	
	
}
