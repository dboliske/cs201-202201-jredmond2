package labs.lab4;

public class Testmain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PhoneNumber number = new PhoneNumber();
		number.setAreaCode("325");
		number.setCountryCode("4");
		number.setNumber("6891642");
		System.out.println(number.toString());
		
		PhoneNumber otherNumber = new PhoneNumber ("12","345","789101112");
		System.out.println(otherNumber);
		System.out.println(otherNumber.isAreaCode());
		System.out.println(otherNumber.isNumber());
		otherNumber.setNumber("7891011");
		System.out.println(otherNumber);
	}

}
