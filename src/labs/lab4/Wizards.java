package labs.lab4;

public class Wizards {

	public static void main(String[] args) {
		potion pot = new potion("Magical",0);
		pot.brew();
		System.out.println(pot.toString());
		pot.setStrength(5);
		pot.brew();
		System.out.println(pot.toString());
		pot.setStrength(10);
		pot.brew();
		System.out.println(pot.toString());
	}

}
