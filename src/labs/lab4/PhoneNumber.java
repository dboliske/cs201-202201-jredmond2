package labs.lab4;

public class PhoneNumber {

	String countryCode;
	String areaCode;
	String number;
	
	public PhoneNumber() {}
	
	public PhoneNumber(String countryCode, String areaCode, String number) {
		this.countryCode = countryCode;
		this.areaCode = areaCode;
		this.number = number;
	}
	
	public String toString() {return countryCode + "-" + areaCode + "-" + number.substring(0,3) + "-" + number.substring(3,number.length());}
	
	public String getCountryCode() {return countryCode;}
	public String getAreaCode() {return areaCode;}
	public String getNumber() {return number;}
	
	public void setCountryCode(String s) {countryCode=s;}
	public void setAreaCode(String s) {areaCode=s;}
	public void setNumber(String s) {number=s;}
	
	public boolean isAreaCode() {if(areaCode.length()==3) {return true;}else {return false;}}
	public boolean isNumber() {if(number.length()==7) {return true;}else {return false;}}
	
	public boolean equals(PhoneNumber pn) {
		if(pn.equals(this)) {return true;}else {return false;}
	}
}
