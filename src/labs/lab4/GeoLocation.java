package labs.lab4;

public class GeoLocation {

	public double lat;
	public double lng;

	GeoLocation() {
	}

	GeoLocation(double lat, double lng) {
		this.lat = lat;
		this.lng = lng;
	}

	public double getLng() {
		return lng;
	}

	public double getLat() {
		return lat;
	}

	public void setLng(int lng) {
		this.lng = lng;
	}

	public void setLat(int lat) {
		this.lat = lat;
	}

	public String toString() {
		return lat + "," + lng;
	}

	public boolean validLat() {
		if (lat > -90.0 && lat < 90.0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean validLng() {
		if (lng > -180.0 && lng < -180.0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean equals(GeoLocation comparison) {
		if (this.equals(comparison)) {
			return true;
		} else {
			return false;
		}
	}
}
