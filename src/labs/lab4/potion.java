package labs.lab4;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class potion {

	File prefixes;
	Scanner reader;
	String effect;
	String prefix;
	double strength;

	public potion() {
		createFiles();
	}

	public potion(String effect, double strength) {
		createFiles();
		this.effect = effect;
		this.strength = strength;
	}

	
	
	public String getEffect() {
		return effect;
	}

	public double getStrength() {
		return strength;
	}

	public String getPrefix() {
		return prefix;
	}

	
	
	public void setEffect(String s) {
		effect = s;
	}

	public void setStrength(double d) {
		strength = d;
	}

	public void setPrefix(String s) {
		prefix = s;
	}

	
	public void brew() {
		double power = (Math.random()*100)*strength;
		int chance = (int)(Math.random()*7);
		if(power<=300) {
		}
		else if(power <= 600) {chance += 8;
		}
		else {chance += 16;
		}
		
		
		int selection = 0;
		while(selection==0 ||selection > 23) {selection = (int)((Math.random()*7)+chance-1);}
		
		
		for(int i=0;i<selection;i++) {
			reader.next();
		}
		prefix = reader.next();
		createFiles();
	}

	
	public String toString() {
		return "The " + prefix + " " + effect +" potion. Potency level: "+strength;
	}

	public boolean validStrength() {
		if (strength > 0 && strength <= 10) {
			return true;
		}
		return false;
	}

	public void createFiles() {
		prefixes = new File("src/labs/lab4/potion prefixes");
		try {
			reader = new Scanner(prefixes);
		} catch (FileNotFoundException f) {
			System.out.println("Potion Prefixes not found");
		}
	}
}
