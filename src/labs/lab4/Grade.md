# Lab 4

## Total

22/30

## Break Down

* Part I GeoLocation
  * Exercise 1-9        7/8
  * Application Class   0/1
* Part II PhoneNumber
  * Exercise 1-9        6.5/8
  * Application Class   1/1
* Part III 
  * Exercise 1-8        7/8
  * Application Class   0.5/1
* Documentation         0/3

## Comments
Part1:
-0.5 Instance variables have to be private
-0.5 Default constructor has to contain default values
-1 Application class missing
Part2:
-0.25 Variables have to be private
-0.5 Default constructor has to contain default values
-0.75 setters should contain the keyword "this" (-0.25 each setter)
Part3:
-0.25 Variables have to be private
-0.25 Default constructor has to contain default values
-0.25 setters should contain the keyword "this" 
-0.25 equals method missing
-class name should be capitalized
-0.5Client class has to contain two objects: one as a default and another as non-default


