package labs.lab0;

public class Square {

	public static void main(String[] args) {
		// create dimension variables and icon char
		int x = 10;
		int y = 10;
		char icon = 'u';

		//first for loop takes care of keeping track of the Y axis
		for (int currY = 1; currY <= y; currY++) {
			//second loop takes care of keeping track of the X axis
			for (int currX = 1; currX <= x; currX++) {
				//testing for if currX or currY is either 1 or equal to their max
				if(currX == 1 || currX == x) {System.out.print(icon);}
				else if(currY == 1 || currY == y) {System.out.print(icon);}
				//creating space in the middle of the square
				else {System.out.print(" ");}
			}
			//line break once X is done
			System.out.println("");
		}
	}

}
