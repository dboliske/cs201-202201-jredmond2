# Lab 0

## Total

19/20

## Break Down

* Eclipse "Hello World" program         5/5
* Correct TryVariables.java & run       4/4
* Name and Birthdate program            5/5
* Square
  * Pseudocode                          2/2
  * Correct output matches pseudocode   2/2
* Documentation                         1/2

## Comments
Good, but not all classes were documented