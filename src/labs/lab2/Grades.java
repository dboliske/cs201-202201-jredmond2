package labs.lab2;

import java.util.Scanner;

public class Grades {
	public static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		boolean sloop = true;
		double totalPoints = 0.0;
		double totalGrades = 0;
		int newGrade;

		while (sloop) {
			newGrade = getInput("Input Grade. Input anything negative to stop.", 0);
			if(newGrade>=0) {
				totalPoints += newGrade;
				totalGrades++;
			}else {sloop = false;}
		}
		print("Grade AVG: "+(totalPoints/totalGrades));
	}

	public static String getInput(String message, String st) {
		print(message + ":");
		return input.next();
	}

	public static int getInput(String message, int st) {
		print(message + ":");
		return input.nextInt();
	}

	public static void print(String message) {
		System.out.println(message);
	}
}
