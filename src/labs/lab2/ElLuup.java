package labs.lab2;

import java.util.Scanner;

public class ElLuup {
	public static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		int number = getInput("Give me a number", 0);

		for (int i = 0; i < number; i++) {
			for (int j = 0; j < number; j++) {
				System.out.print(":)");
			}
			print("");
		}
	}

	public static String getInput(String message, String st) {
		print(message + ":");
		return input.next();
	}

	public static int getInput(String message, int st) {
		print(message + ":");
		return input.nextInt();
	}

	public static void print(String message) {
		System.out.println(message);
	}
}
