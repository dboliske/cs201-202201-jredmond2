package labs.lab2.TheFiresOfAncientCosmicDestiny;

public class combatant {
	String name;
	int MaxHealth;
	int health;
	int level;
	int ATK;
	int DEF;
	int MP;
	int ATKBoost;
	int DEFBoost;
	int phase;
	
	combatant(){}
	
	public void cooldown() {};
	
	public void restore() {
		health = MaxHealth;
	}
	
	public String ability(combatant target) {return null;}
	
	public String attack(combatant target) {
		int damage = this.ATK - target.DEF;
		if(damage<0) {damage = 0;}
		
		target.health -= (damage);
	
		return this.name+" attacks "+target.name+" dealing "+(damage)+" damage.";
	}
	
	//for Zartothrax only
	public int getPhase() {return phase;}
	
	//for the finale
	public String hoots(combatant enemy) {return "hoot";}

}
