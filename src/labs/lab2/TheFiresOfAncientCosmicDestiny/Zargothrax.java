package labs.lab2.TheFiresOfAncientCosmicDestiny;

public class Zargothrax extends combatant {

	int phase = 1;
	public int getPhase() {return phase;}

	Zargothrax() {
		super.name = "Zargothrax";
		super.MaxHealth = 10000;
		super.health = MaxHealth;
		super.ATK = 200;
		super.DEF = 200;
		super.level = 99;
		super.ATKBoost = 0;
		super.DEFBoost = 0;
	}

	public String attack(combatant target) {

		int moveGen;
		int damage;
		int currentPhase = phase;

		phase = checkPhase();
		moveGen = (int) (Math.random() * 20);
		switch (phase) {
		// phase 1 attacks

		case 1:

			// Zargothrax's basic attack, 40% chance to activate in Phase 1
			if (moveGen <= 8) {

				damage = this.ATK - target.DEF;
				if (damage < 0) {
					damage = 0;
				}

				target.health -= (damage);
				if(target.health < 0) {target.health=0;}
				return this.name + " fires a ball of Galactic Energy at " + target.name + " dealing " + (damage)
						+ " damage.";
			}
			// Zargothrax's secondary attack, dealing 250% ATK 20% chance to occur in Phase
			// 1
			else if (moveGen > 8 && moveGen <= 12) {
				damage = (int)(this.ATK * 2.5) - target.DEF;
				if (damage < 0) {
					damage = 0;
				}

				target.health -= damage;
				if(target.health < 0) {target.health=0;}
				return this.name + " stabs " + target.name + " with an unholy pike of evil. Dealing " + damage
						+ " damage.";

			}
			// Zargothrax's minor ATK boost
			else if (moveGen > 14 && moveGen <= 17) {
				this.ATKBoost += 25;
				this.ATK += 25;
				return this.name + " channels dark energy from the Knife of Evil into him. " + this.name
						+ "'s ATK raises.";
			}
			// Zargothrax's minor DEF boost
			else if (moveGen > 17 && moveGen <= 20) {
				this.DEFBoost += 20;
				this.DEF += 20;
				return this.name + " creates a dark energy shield, reinforced by his heart of pure evil. " + this.name
						+ "'s DEF raises.";
			}
			// Angus McFife DEF reduction
			else if (moveGen > 12 && moveGen <= 14) {
				damage = (int)(120) - target.DEF;
				if (damage < 0) {
					damage = 0;
				}

				target.health -= damage;
				if(target.health < 0) {target.health=0;}
				target.DEFBoost -= 20;
				target.DEF -= 20;
				return "A survivor from the Death Knights of Crail throws a Plasma Grenade at " + target.name
						+ " decreasing DEF and dealing "+damage+" damage.";
			}

			// phase 2 attacks
		case 2:
			// p2 initiation ability
			if (currentPhase != 2) {
				target.ATKBoost -= 100;
				target.ATK -= 100;
				target.DEFBoost -= 100;
				target.DEF -= 100;
				this.ATKBoost = 50;
				this.ATK += 50;
				this.DEFBoost = 50;
				this.DEF += 50;
				return "\"YOU ARE NAUGHT BUT INSIGNIFICANT WORMS BEFORE THE INFINITE POWER OF ZARGOTHRAX!\"\n "
						+ target.name + "'s ATK and DEF was greatly decreased!\n"
								+ this.name+"'s ATK and DEF was greatly increased!";
			}
			// p2 basic attack 40% chance to activate
			if (moveGen <= 8) {

				damage = (int) (this.ATK * 1.5) - target.DEF;
				if (damage < 0) {
					damage = 0;
				}

				target.health -= (damage);
				if(target.health < 0) {target.health=0;}
				return this.name + " launches a mystic missile at " + target.name + " dealing " + (damage) + " damage.";
			}
			// p2 secondary attack, dealing 4x damage and increasing ATK, 20% chance to
			// activate
			if (moveGen <= 12 && moveGen > 8) {

				damage = (int) (this.ATK * 4) - target.DEF;
				if (damage < 0) {
					damage = 0;
				}
				this.ATK += 100;
				this.ATKBoost += 100;
				target.health -= (damage);
				if(target.health < 0) {target.health=0;}
				return this.name + " summons a mighty lightning bolt to thrash down upon " + target.name + " dealing "
						+ (damage) + " and increasing ATK";
			}
			// p2 tertiary attack, ignoring DEF, lowering it, 10% chance
			if (moveGen <= 14 && moveGen > 12) {

				damage = (int) (this.ATK);
				if (damage < 0) {
					damage = 0;
				}
				target.health -= (damage);
				if(target.health < 0) {target.health=0;}
				target.DEF -= 100;
				target.DEFBoost -= 100;
				return this.name + " fires a piercing beam of light towards " + target.name
						+ ", ignoring and shattering defense, dealing " + (damage);
			}
			// 500 health heal if below 3000 HP. DEF rise if above 3000. 10% chance
			if (moveGen <= 16 && moveGen > 14) {
				if (this.health >= 3000) {
					this.DEF += 70;
					this.DEFBoost += 70;
					return this.name + " channels the power of the stars, increasing DEF.";
				} else {
					this.health += 500;
					return this.name + " channels the power of the stars, restoring lifeforce.";
				}
			}
			// combines DEF into ATK and attacks Angus. DEF is left at 0. This attack cannot
			// occur if DEF < 100
			// If DEF < 100, the final attack will occur instead
			if (moveGen <= 17 && moveGen > 16) {
				if (this.DEF >= 100) {
					damage = (int) (this.ATK + this.DEF) - target.DEF;
					if (damage < 0) {
						damage = 0;
					}
					target.health -= (damage);
					if(target.health < 0) {target.health=0;}
					this.DEF = 0;
					this.DEFBoost = 0;
					return this.name + " relinquishes his defensive power into a mighty attack, dealing " + damage
							+ " to " + target.name + ".\n All of " + this.name + "'s DEF was sacrificed in the attack.";
				}

			}
			// Greatly increases ATK and DEF
			if (moveGen <= 20 && moveGen > 16) {
				this.DEF += 50;
				this.DEFBoost += 50;
				this.ATK += 50;
				this.ATKBoost += 50;
				return this.name + "'s veins are charging with Astral Fire. ATK and DEF are increased.";
			}

			// phase 3 attacks
		case 3:
			// p3 initiation ability
			if (currentPhase != 3) {
				this.health = this.MaxHealth;
				this.ATK = 500;
				this.DEF = 300;
				this.ATKBoost = 0;
				this.DEFBoost = 0;
				return "\"DO YOU THINK ARE YOU CAPABLE OF DEFEATING ME? THE ALL-POWERFUL LORD ZARGOTHRAX?\"\n\"I AM UNDYING AND ETERNAL! WORSHIP MY OMINIPOTIENCE!";
			}

		default:
			return null;
		}

	}

	public int checkPhase() {
		
		if (this.health <= this.MaxHealth * .5 && phase != 3 && this.health > 0) {
			return 2;
		} 
		else if (this.health <= 0 && phase != 3) {
			this.health = 0;
			return 3;
		}
		return 1;
	}

}