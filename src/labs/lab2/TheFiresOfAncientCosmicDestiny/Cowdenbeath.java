package labs.lab2.TheFiresOfAncientCosmicDestiny;

import java.util.Scanner;

public class Cowdenbeath {

	// you were about to do something with the menu options
	public static Scanner input = new Scanner(System.in);

	public static String heroMessage = "Angus McFife XIII stands off against the might of the evil Lord Zargothrax.\nHe plans his first move...";
	public static String enemyMessage = "";

	public static boolean HeroTurn = true;

	public static combatant player = new AngusMcFife();
	public static combatant enemy = new Zargothrax();

	public static int turnCount = 1;

	public static void main(String[] args) {

		String[] combatOptions = { "Attack", "Abilities" };
		String[] hootsOptions = { "Bringforth the power of Hoots" };

		boolean warIsFight = true;

		print("Enter Something. Eclipse doesn't have a console clear function so just... type something and let the psuedo clear work.");
		input.next();

		while (warIsFight) {

			updateHUD(player, enemy);

			if (enemy.getPhase() == 3) {
				combatOptions = hootsOptions;
			}

			if (HeroTurn && player.health > 0) {
				// Angus' turn

				heroMessage = combatOptions(printOptions(combatOptions), player, enemy);
				enemyMessage = "";
			} else if (HeroTurn && player.health <= 0) {
				heroMessage = "Angus McFife XIII lays defeated before the mighty Lord Zargothrax... Dundee is doomed...";
				updateHUD(player, enemy);
				next();
				print("Game Over");
				warIsFight = false;
			} else if (enemy.getPhase() != 3) {

				// Zargothrax's turn
				player.cooldown();
				enemyMessage = enemy.attack(player);
				heroMessage = "";
				next();
				
			} else if (enemy.getPhase() == 3) {
				heroMessage = combatOptions(printOptions(combatOptions), player, enemy);
				enemy.health = 0;
				heroMessage = "Angus McFife and The Hootsman deal 999999 damage to Zargothrax.";
				updateHUD(player, enemy);
				next();
				clear();
				print("And lo, Zargothrax was finally defeated.\n"
						+ " But in the epic final struggle with the dark sorcerer, the valiant hero Angus McFife realised he had been impaled by the Knife of Evil.\n"
						+ "As he watched the lifeless corpse of Zargothrax dissolve into liquid dust, Angus knew that soon he himself would be corrupted by the dark power of that cursed blade.\n"
						+ "And so he resolved that he must make the final sacrifice for the Glory of Dundee...he would end his own life by descending into the raging volcanic fires of Schiehallion!\n");
				next();
				clear();
				print("\n\"Goodbye\r\n" + "My time has come to die\r\n" + "This is the end\r\n"
						+ "I must say farewell\r\n" + "Descend in to hell..\"\n");
				next();
				clear();
				print("In the fires of ancient cosmic destiny\r\n" + "Evil will rise\r\n"
						+ "And of all the legendary warriors\r\n" + "None will survive\r\n"
						+ "In the fires of ancient cosmic destiny\r\n" + "Evil will rise\r\n"
						+ "To defeat the power of the sorcerer\r\n" + "The Prince of Fife must Die\r\n"
						+ "To save the galaxy\r\n" + "It is your mighty destiny\r\n" + "Brave Angus McFife!\n");
				next();
				clear();
				print("The End.");
				warIsFight = false;
			}
			if (warIsFight) {
				HeroTurn = toggleTurn(HeroTurn);
			}

		}

	}

	public static void updateHUD(combatant player, combatant enemy) {
		String enemyBuffString = "";
		String heroBuffString = "";

		if (enemy.health < 0) {
			enemy.health = 0;
		}
		if (player.health < 0) {
			player.health = 0;
		}

		if (enemy.ATKBoost > 0) {
			enemyBuffString += "  [ATK UP]";
		} else if (enemy.ATKBoost < 0 && enemy.ATK >= 0) {
			enemyBuffString += "  [ATK DOWN]";
		} else if (enemy.ATKBoost < 0 && enemy.ATK <= 0) {
			enemyBuffString += "  [ATK VULNERABLE]";
		}

		if (enemy.ATKBoost >= 50 || enemy.ATKBoost <= -50) {
			enemyBuffString += "*";
		}
		if (enemy.ATKBoost >= 100 || enemy.ATKBoost <= -100) {
			enemyBuffString += "*";
		}
		if (enemy.ATKBoost >= 200 || enemy.ATKBoost <= -200) {
			enemyBuffString += "*";
		}
		if (enemy.ATKBoost >= 300 || enemy.ATKBoost <= -300) {
			enemyBuffString += "*";
		}
		if (enemy.ATKBoost >= 500 || enemy.ATKBoost <= -500) {
			enemyBuffString += "*";
		}

		if (enemy.DEFBoost > 0) {
			enemyBuffString += "  [DEF UP]";
		} else if (enemy.DEFBoost < 0) {
			enemyBuffString += "  [DEF DOWN]";
		} else if (enemy.DEFBoost < 0 && enemy.DEF <= 0) {
			enemyBuffString += "  [DEF VULNERABLE]";
		}

		if (enemy.DEFBoost >= 50 || enemy.DEFBoost <= -50) {
			enemyBuffString += "*";
		}
		if (enemy.DEFBoost >= 100 || enemy.DEFBoost <= -100) {
			enemyBuffString += "*";
		}
		if (enemy.DEFBoost >= 200 || enemy.DEFBoost <= -200) {
			enemyBuffString += "*";
		}
		if (enemy.DEFBoost >= 300 || enemy.DEFBoost <= -300) {
			enemyBuffString += "*";
		}
		if (enemy.DEFBoost >= 500 || enemy.DEFBoost <= -500) {
			enemyBuffString += "*";
		}

		if (player.ATKBoost > 0) {
			heroBuffString += "  [ATK UP]";
		} else if (player.ATKBoost < 0 && player.ATK >= 0) {
			heroBuffString += "  [ATK DOWN]";
		} else if (player.ATKBoost < 0 && player.ATK <= 0) {
			heroBuffString += "  [ATK VULNERABLE]";
		}

		if (player.ATKBoost >= 50 || player.ATKBoost <= -50) {
			heroBuffString += "*";
		}
		if (player.ATKBoost >= 100 || player.ATKBoost <= -100) {
			heroBuffString += "*";
		}
		if (player.ATKBoost >= 200 || player.ATKBoost <= -200) {
			heroBuffString += "*";
		}
		if (player.ATKBoost >= 300 || player.ATKBoost <= -300) {
			heroBuffString += "*";
		}
		if (player.ATKBoost >= 500 || player.ATKBoost <= -500) {
			heroBuffString += "*";
		}

		if (player.DEFBoost > 0) {
			heroBuffString += "  [DEF UP]";
		} else if (player.DEFBoost < 0) {
			heroBuffString += "  [DEF DOWN]";
		} else if (player.DEFBoost < 0 && enemy.DEF <= 0) {
			heroBuffString += "  [DEF VULNERABLE]";
		}

		if (player.DEFBoost >= 50 || player.DEFBoost <= -50) {
			heroBuffString += "*";
		}
		if (player.DEFBoost >= 100 || player.DEFBoost <= -100) {
			heroBuffString += "*";
		}
		if (player.DEFBoost >= 200 || player.DEFBoost <= -200) {
			heroBuffString += "*";
		}
		if (player.DEFBoost >= 300 || player.DEFBoost <= -300) {
			heroBuffString += "*";
		}
		if (player.DEFBoost >= 500 || player.DEFBoost <= -500) {
			heroBuffString += "*";
		}

		clear();
		println(enemy.name + "\nHEALTH: " + enemy.health);
		println(enemyBuffString);
		println(enemyMessage + "\n");
		println(player.name + ":\nHEALTH: " + player.health + "\nCHARGE: " + player.MP + "/100");
		println(heroBuffString);
		println(heroMessage + "\n");

	}

	public static int printOptions(String[] options) {
		for (int i = 1; i < options.length + 1; i++) {
			println(i + "/" + options[i - 1]);
		}
		return input.nextInt();
	}

	public static void next() {
		print("1/Next");
		input.next();
	}

	public static boolean toggleTurn(boolean turn) {
		if (turn) {
			return false;
		} else {
			return true;
		}
	}

	public static String combatOptions(int option, combatant player, combatant enemy) {
		if (enemy.getPhase() != 3) {
			switch (option) {
			case 1:

				return player.attack(enemy);
			case 2:
				String statement = player.ability(enemy);
				if (statement.equals("return")) {
					HeroTurn = toggleTurn(HeroTurn);
					return "";
				} else if (statement.equals("lack")) {
					HeroTurn = toggleTurn(HeroTurn);
					return "Insufficient Charge";
				} else if (statement.equals("cooldown")) {
					HeroTurn = toggleTurn(HeroTurn);
					return "This ability is on Cooldown";
				} else {

					return statement;
				}
			case 3:
				return null;
			}
			return null;
		} else
			switch (option) {
			default:
				return player.hoots(enemy);
			}
	}

	public static void print(String message) {
		System.out.print(message);
	}

	public static void println(String message) {
		System.out.println(message);
	}

	public static void clear() {
		for (int i = 0; i < 100; i++) {
			println("");
		}
	}
}
