package labs.lab2.TheFiresOfAncientCosmicDestiny;

import java.util.Scanner;

public class AngusMcFife extends combatant {

	Scanner input = new Scanner(System.in);

	AngusMcFife() {
		super.name = "Angus McFife XIII";
		super.MaxHealth = 5000;
		super.health = MaxHealth;
		super.ATK = 220;
		super.DEF = 80;
		super.level = 99;
		super.MP = 0;
		super.ATKBoost = 0;
		super.DEFBoost = 0;
	}

	double MPModifier = 500.0;
	double MPGainModifier = 30.0;
	double MPGainBase = 5;
	
	String[] abilityNames = { "Powered By Lasers! [12]", "Calm of the Unicorn [15]", "Ralathor's Support [16]",
			"Prolitius' Support [16]", "Oath of the Spaceknights [22]", "The Ride of the Questlords of Inverness [28]", "Hootsforce Barrage! [30]",
			"Power of the Laser Dragon Fire! [35]", "Strike of Triton! [35]", "In Hoots we Trust! [60]",
			"Heavy! Metal! Dark Lord! Crasher! [75]", "Cancel" };
	String[] abilities = abilityNames;
	int[] abilityCD = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

	public void cooldown() {
		for (int i = 0; i < abilityCD.length; i++) {
			if (abilityCD[i] > 0) {
				abilityCD[i] = abilityCD[i] - 1;
			}
		}
	}

	public String attack(combatant target) {

		int damage = (int) (this.ATK * (1 + (MP / MPModifier)) - target.DEF);
		if (damage < 0) {
			damage = 0;
		}

		target.health -= (damage);

		MP += (int) (damage / MPGainModifier) + MPGainBase;

		if (MP > 100) {
			MP = 100;
		}

		return this.name + " attacks " + target.name + " dealing " + (damage)
				+ " damage. He feels the strength in the Hammer of Glory returning to him.";
	}

	public String ability(combatant target) {

		boolean lack = false;

		for (int i = 1; i < abilities.length + 1; i++) {
			System.out.print(i + "/" + abilities[i - 1]);
			if(abilityCD[i-1]>0) {System.out.print(" - On Cooldown ("+abilityCD[i-1]+")");}
			System.out.print("\n");
		}

		int choice = input.nextInt();
		switch (choice) {
		case 1:
			// Powered by Lasers! Deals 200% of ATK damage to Zargothrax
			if (abilityCD[0] != 0) {
				return "cooldown";
			}
			if (MP >= 12 && choice == 1 && abilityCD[0] == 0) {
				int damage = (int) ((this.ATK * 2.5) * (1 + (MP / MPModifier)) - target.DEF);
				if (damage < 0) {
					damage = 0;
				}
				target.health -= (damage);
				if (target.health < 0) {
					target.health = 0;
				}

				MP -= 12;
				abilityCD[0] = 3;
				return this.name + " fires a barrage of lasers from the Hammer of Glory dealing " + damage
						+ " damage to " + target.name;
			} else {
				lack = true;
			}
		case 2:
			// Heals 750 health, plus 150% of ATK to self
			if (abilityCD[1] != 0) {
				return "cooldown";
			}
			if (MP >= 15 && choice == 2 && abilityCD[1] == 0) {
				int heal = (int) ((this.ATK * 1.5) + 750);
				this.health += heal;
				if(this.health>this.MaxHealth) {this.health=this.MaxHealth;}
				MP -= 15;
				abilityCD[1] = 6;
				return this.name + " reminisces back to the Land of Unicorns. He heals " + heal + " health";
			} else {
				lack = true;
			}
		case 3:
			// Increases DEF moderately
			if (abilityCD[2] != 0) {
				return "cooldown";
			}
			if (MP >= 16 && choice == 3 && abilityCD[2] == 0) {

				this.DEF += 40;
				this.DEFBoost += 40;

				MP -= 16;
				abilityCD[2] = 9;
				return "Commander Ralathor is summoned onto the battlefield. He grants " + this.name
						+ " a digital shield for the epic battle! DEF raised!";
			} else {
				lack = true;
			}
		case 4:
			// Increases ATK moderately
			if (abilityCD[3] != 0) {
				return "cooldown";
			}
			if (MP >= 16 && choice == 4 && abilityCD[3] == 0) {

				this.ATK += 60;
				this.ATKBoost += 60;

				MP -= 16;
				abilityCD[3] = 9;
				return "The Holographic warrior Prolitius is summoned onto the battlefield. He threatens to encase Zargothrax within a tomb of unmelting ice once the battle ends.\n"
						+ this.name + "'s morale is boosted! ATK raised!";
			} else {
				lack = true;
			}
		case 5:
			// Decrease Zargothrax's ATK and DEF by 40
			if (abilityCD[4] != 0) {
				return "cooldown";
			}
			if (MP >= 22 && choice == 5 && abilityCD[4] == 0) {

				target.ATK -= 60;
				target.ATKBoost -= 60;
				target.DEF -= 40;
				target.DEFBoost -= 40;

				MP -= 22;
				abilityCD[4] = 9;
				return "The sheer determination of "+this.name+" sheds fear into the eyes of "+target.name+". ATK and DEF lowered.\n";
			} else {
				lack = true;
			}
		case 6:
			// Deals 300% ATK damage to Zargothrax and lowers his DEF by 60
			if (abilityCD[5] != 0) {
				return "cooldown";
			}
			if (MP >= 28 && choice == 6 && abilityCD[5] == 0) {

				int damage = (int) ((this.ATK * 3) * (1 + (MP / MPModifier)) - target.DEF);
				if (damage < 0) {
					damage = 0;
				}
				target.health -= (damage);
				if (target.health < 0) {
					target.health = 0;
				}
				target.DEF -= 60;
				target.DEFBoost -= 60;
				MP -= 28;
				abilityCD[5] = 12;
				return "The spirits of the fallen Questlords resonate within " + this.name
						+ ". \nWith the power of the spirits, he charges forward and pummels " + target.name
						+ " with a mighty blow, dealing " + damage + " damage and lowering DEF!";
			} else {
				lack = true;
			}
		case 7:
			// Deals 350% ATK damage to Zargothrax, ignoring DEF
			if (abilityCD[6] != 0) {
				return "cooldown";
			}
			if (MP >= 30 && choice == 7 && abilityCD[6] == 0) {

				int damage = (int) ((this.ATK * 3.5) * (1 + (MP / MPModifier)));
				if (damage < 0) {
					damage = 0;
				}
				target.health -= (damage);
				if (target.health < 0) {
					target.health = 0;
				}
				MP -= 30;
				abilityCD[6] = 13;
				return "The Hootsforce fire an array of missiles at the Dark Lord at the command of " + this.name
						+ ", piercing through " + target.name + "'s defenses, dealing " + damage+" damage.";
			} else {
				lack = true;
			}
		case 8:
			// Increases ATK and DEF heavily
			if (abilityCD[7] != 0) {
				return "cooldown";
			}
			if (MP >= 35 && choice == 8 && abilityCD[7] == 0) {

				this.ATK += 65;
				this.DEF += 45;
				this.ATKBoost += 65;
				this.DEFBoost += 45;

				MP -= 35;
				abilityCD[7] = 12;
				return this.name
						+ " charges the strength of his Legendary Enchanted Jetpack. He feels the flames of the Laser Dragon Fire burning within him.\nATK and DEF greatly increased.";
			} else {
				lack = true;
			}
		case 9:
			// Deals 300% ATK to Zargothrax, decreasing his ATK heavily
			if (abilityCD[8] != 0) {
				return "cooldown";
			}
			if (MP >= 35 && choice == 9 && abilityCD[8] == 0) {
				int damage = (int) ((this.ATK * 3) * (1 + (MP / MPModifier)) - target.DEF);
				if (damage < 0) {
					damage = 0;
				}
				target.health -= (damage);
				target.ATK -= 100;
				target.ATKBoost -= 100;
				if (target.health < 0) {
					target.health = 0;
				}

				MP -= 35;
				abilityCD[8] = 12;
				return this.name
						+ " swings the Hammer of Glory with flaming might. His power enough to send Zargothrax back to the planet of knights!\n It deals "
						+ damage + " damage, heavily lowering " + target.name + "'s ATK!";
			} else {
				lack = true;
			}
		case 10:
			// Restores buffs if below 0. Heals 3000 health and boosts DEF by 100.
			if (abilityCD[9] != 0) {
				return "cooldown";
			}
			if (MP >= 60 && choice == 10 && abilityCD[9] == 0) {
				int heal = 3000;
				this.health += heal;
				if(this.health>this.MaxHealth) {this.health=this.MaxHealth;}
				if (this.ATKBoost < 0) {
					this.ATKBoost = 0;
					this.ATK = 220;
				}
				if (this.DEFBoost < 0) {
					this.DEFBoost = 0;
					this.DEF = 80;
				}
				
				this.DEF+=100;
				this.DEFBoost+=100;

				MP -= 60;
				abilityCD[9] = 30;
				return this.name
						+ " keeps his faith in the power of Hoots. His weaknesses are cleared, DEF heavily risen, and health restored by "
						+ heal;
			} else {
				lack = true;
			}
		case 11:
			// Deals 500% ATK to Zargothrax, decreasing his DEF and ATK heavily
			if (abilityCD[10] != 0) {
				return "cooldown";
			}
			if (MP >= 75 && choice == 11) {
				int damage = (int) ((this.ATK * 5) * (1 + (MP / MPModifier)) - target.DEF);
				if (damage < 0) {
					damage = 0;
				}
				target.health -= (damage);
				target.DEF -= 150;
				target.DEFBoost -= 150;
				target.ATK -= 200;
				target.ATKBoost -= 200;

				if (target.health < 0) {
					target.health = 0;
				}

				MP -= 75;
				abilityCD[10] = 30;
				return "\"MY STEEL WILL BLOW YOU AWAY! MIGHTY PRINCE WITH A LICENSE TO SLAY! FIGHTING MEGABATTLES EVERYDAY!\""
						+ "\n With a mighty blow of the Gloryhammer! " + target.name + " takes " + damage + " damage!";
			} else {
				lack = true;
			}
		default:
			if (lack) {
				return "lack";
			}
			return "return";
		}
	}

	public String hoots(combatant target) {
		return "\nFROM THE HEAVENS\n" + "COMES A HERO\n" + "MIGHTY HOOTSMAN!\n" + "Vanitati\n" + "Latinae\n"
				+ "Canentis\n" + "Hootus Dei\n" + "\nSIDE BY SIDE WITH ANGUS!\n" + "THE WARRIORS MAKE FIGHT!\n"
				+ "HOOTSMAN FIRES A LASER BLAST!\n" + "SHOOTS GOBLINS OUT OF SIGHT!\n\n"
				+ "\"ALRIGHT ZARGOTHRAX! YOUR EVIL REIGN OF TERROR ENDS TODAY!\"\n"
				+ "\"MY NAME, IS THE HOOTSMAN! AND I AM THE ONLY TRUE GOD OF THIS UNIVERSE!\"\n"
				+ "\"IT IS TIME FOR YOU TO TASTE THE POWER OF HOOTS!\"\n\n" + "With the force of the hammer\n"
				+ "And the power of Hoots\n" + "The forces of justice attack\n"
				+ "Bringing destruction to Zargothrax\r\n";
	}

}
