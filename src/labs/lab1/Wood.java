package labs.lab1;

import java.util.Scanner;

public class Wood {
	public static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		int[] lwh = new int[3];
		lwh[0] = getInput("Enter length of box",1);
		lwh[1] = getInput("Enter width of box",2);
		lwh[2] = getInput("Enter height of box",3);
		
		print("Box will take "+((2*(lwh[0]*lwh[2]+lwh[1]*lwh[2]))+(lwh[0]*lwh[1]))+" cm^2 of woodliness.");
	}

	public static String getInput(String message, String st) {
		print(message + ":");
		return input.next();
	}

	public static int getInput(String message, int st) {
		print(message + ":");
		return input.nextInt();
	}

	public static void print(String message) {
		System.out.println(message);
	}
}
