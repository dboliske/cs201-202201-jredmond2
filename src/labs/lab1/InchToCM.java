package labs.lab1;

import java.util.Scanner;

public class InchToCM {
	public static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		int inches = getInput("Input inches", 1);
		print("That is "+inches*2.54+"cm.");
	}

	public static String getInput(String message, String st) {
		print(message + ":");
		return input.next();
	}

	public static int getInput(String message, int st) {
		print(message + ":");
		return input.nextInt();
	}

	public static void print(String message) {
		System.out.println(message);
	}
}
