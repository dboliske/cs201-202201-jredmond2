package labs.lab1;

import java.util.Scanner;

public class FtoC {

	public static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		double temp = getInput("Insert a tempreture, either F or C", 0.01);
		print("Assuming you typed in Farenheit, it translates to "+(double)((temp-32)*(.555))+" degrees Celcius.");
		print("Assuming you typed in Celcius, it translates to "+((double)(temp+32)/(.555))+" degrees Farenheit.");
	}

	public static String getInput(String message, String st) {
		print(message + ":");
		return input.next();
	}

	public static double getInput(String message, double st) {
		print(message + ":");
		return input.nextDouble();
	}

	public static void print(String message) {
		System.out.println(message);
	}
}
