# Lab 1

## Total

10/20

## Break Down

* Exercise 1    1.5/2
* Exercise 2    1.5/2
* Exercise 3    1.5/2
* Exercise 4
  * Program     1.5/2
  * Test Plan   0/1
* Exercise 5
  * Program     2/2
  * Test Plan   0/1
* Exercise 6
  * Program     2/2
  * Test Plan   0/1
* Documentation 0/5

## Comments

-2 points for late submission.

Please read the comments very carefully:
1. First of all, you submitted your lab 2 days late I am supposed to take off 2 points for each late day, that is 4 points total. I will only take off 2 points this time. However, next time you will be disadvanteged from two points for each late day.
2. You were asked to write 6 different classes answering 6 questions. You only did 4. To make sure about the number of classes and questions, review the ReadMe from bitbucket. It is clearer there.
3. Please give a clearer name to each of your classes in the next labs (exercise1 , exercise2 etc.. ) so it's easy to grade you.
4. You did not document/ comment any of your code which is 5 points alone.

-As I mentioned, you combined exercise1, exercise2 and exercise3 together which affected the grade of your programs for these 3 exercises.
-For exercise 4: - You were asked to prompt the user twice: the first time is for the temp in faranheit and the second time for the temp in celcius. You shouldn't have combined them and print a message that assumes something.
                 - The formula you used to convert from a unit to another is wrong.
                 - You did not do your test cases / the test table.
                 
-For exercise 5: - You were asked to take inputs in inches and to display result in feet. To do that, you could have divised the operation by 144.
                 - You did not do your test cases / the test table
                 
- For exercise 6: - You did not do your test cases / the test table
Again, please restrict to the question. It is good to see a different code, but writing unnecesary code without completing the required tasks is not to anyone's benefit.