package labs.lab1;

import java.util.Scanner;

public class InputAndOutput {

	public static Scanner input = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		int age = getInput("What is your age?", 1);
		print("You are "+(getInput("What is your father's age?", 0) - age)+" years younger than your dad.");
		int inches = getInput("How tall are you in inches?", 0);
		print("You are "+(inches*2.54)+"cms tall.");
		print("You are "+(int)(inches/12)+"ft. and "+(inches%12)+"in. tall.");
		print("The first letter of your name is "+getInput("What is your name?","").charAt(0)+".");
	}

	public static String getInput(String message, String st) {
		print(message+":");
		return input.next();
	}
	
	public static int getInput(String message, int st) {
		print(message+":");
		return input.nextInt();
	}
	
	public static void print(String message) {
		System.out.println(message);
	}
}
