package labs.lab6;
//asfsdafasdfa
public class customer {

	String name;
	int sausage;
	boolean zombie = false;
	
	public customer() {}
	
	public customer(String n, int s) {
		name = n; sausage = s;
	}
	
	public String getName() {
		if(zombie) {
			return name +" (Zombie) ";
		}
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSausage() {
		return sausage;
	}

	public void setSausage(int sausage) {
		this.sausage = sausage;
	}

	public boolean isZombie() {
		return zombie;
	}

	public void setZombie(boolean zombie) {
		this.zombie = zombie;
	}
	
	public String toString() {
		String label = "";
		label += getName() +"";
		label += "\n  Sausage Count: "+sausage;
		
		return label; 
	
	}
	
}
