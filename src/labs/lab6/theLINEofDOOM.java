package labs.lab6;

import java.util.ArrayList;
import java.util.Scanner;

public class theLINEofDOOM {

	public static boolean yes = true;
	public static boolean target = true;
	public static Scanner input = new Scanner(System.in);
	public static ArrayList<customer> line = new ArrayList<customer>();
	public static customer tempCustomer;
	public static int sausageStock = 20;
	public static int cash = 0;
	public static final int sausagePrice = 2;
	public static final int sausageSell = 5;
	//fdjlsakjfsdlakfjldsafjlsdajflsd
	public static void main(String args[]) {
		while (yes) {
			System.out.println("Sausages: " + sausageStock + "\nCash: " + cash + "\n");
			System.out.println(
					"Hello, CEO of lines. Choose an option and play God while you're at it:\n1/Add Customer\n2/Remove Customer\n3/Help Next Customer\n4/Initiate Nuclear Warfare with Target\n5/View Line\n6/Restock\n7/Exit\n");
			switch (input.nextInt()) {
			case 1:

				tempCustomer = addCustomer();

				if (!target && Math.random() > .8) {
					System.out.println(
							tempCustomer.getName() + " is a nuclear zombie. Their sausage count has been reduced.");
					tempCustomer.setSausage(tempCustomer.getSausage() / 2);
					tempCustomer.setZombie(true);
				}

				line.add(tempCustomer);
				System.out.println(tempCustomer.getName() + " was added to the line at spot " + line.size() + ".\n");
				break;

			case 2:
				System.out.println("Enter customer name:");
				tempCustomer = removeCustomer(input.next());

				if (tempCustomer != null) {
					line.remove(tempCustomer);
					System.out.println(tempCustomer.name + " has been dealt with.\n");
					sausageStock += tempCustomer.getSausage();
					break;
				}
				break;

			case 3:
				if (line.size() > 0) {
					tempCustomer = line.get(0);
					System.out.println(tempCustomer.getName() + " has paid for their items and has bought "
							+ tempCustomer.getSausage() + " sausages.");
					
					System.out.println("+$"+tempCustomer.getSausage()*sausageSell+"\n");
					cash += tempCustomer.getSausage()*sausageSell;
					
					line.remove(tempCustomer);
					break;
				}
				System.out.println(
						"Markus the Janitor waves to you with a pathetic smile. He is the only other person in the store.\n");
				break;
			case 6:
				int sausages = 0;
				int cost = 0;
				while(sausageStock < 20 || cash > sausagePrice) {
					sausages++; cost += sausagePrice;
					cash -= sausagePrice; sausageStock++;
				}
				System.out.println("You purchased "+sausages+" sausages and spent $"+cost);
				break;
			case 4:

				nukeTheTarget();
				break;

			case 5:

				for (customer c : line) {
					System.out.println(c);
				}
				break;
			case 7:
				yes = false;
				if(target) {System.out.println("Target got to you first...");}
				else {System.out.println("Target nuked you back...");}
				break;

			}
		}
	}

	public static customer addCustomer() {
		String n;
		int s;

		System.out.println("\nEnter name of customer:");
		n = input.next();
		System.out.println("Enter preferred sausage amount:");
		s = input.nextInt();

		if (sausageStock - s < 0) {
			System.out.println("You do not have enough sausages for their taste. They take less sausages.");
			s = sausageStock;
		}

		sausageStock -= s;

		return new customer(n, s);
	}

	public static customer removeCustomer(String n) {
		for (customer c : line) {
			if (c.getName().equals(n)) {
				return c;
			}
		}
		System.out.println("No customer found.");
		return null;
	}

	public static boolean nukeTheTarget() {
		if (target) {
			target = false;
			System.out.println("The Target has been nuked\n");
		} else {
			System.out.println("You already nuked the Target\n");
		}
		return target;
	}
}
